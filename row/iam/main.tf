terraform {
  backend "s3" {
    bucket  = "terraform-state-bucket-jeffin"
    key     = "myinfra/us-east-1/infra.tfstate"
    region  = "us-east-1"
  }
}

resource "aws_s3_bucket" "my_bucket" {
  bucket = "test-bucket-jeffin-joseph"

  tags = {
    Name        = "My bucket"
    Environment = "prod"
  }
}

resource "aws_s3_bucket_acl" "s3_acl" {
  bucket = aws_s3_bucket.my_bucket.id
  acl    = "private"
}

resource "aws_iam_user" "my_iam_user" {
  name = "kevin"
}

resource "aws_iam_user_policy" "s3_iam_policy" {
  name = "s3_iam_policy"
  user = aws_iam_user.my_iam_user.name

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:GetObject",
          "s3:GetBucketLocation",
          "s3:ListBucket"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

output "S3_bucket_details"{
  value = aws_s3_bucket.my_bucket
}


